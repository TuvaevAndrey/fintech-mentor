# fintech-mentor

## Cервис для матчинга эксперта и того, кто хочет эту экспертизу получить. Такой "Tinder" для менторства.

## Модель данных
Ментор:
- фио
- проект
- список навыков
- способ связи (напр номер телефона или идентификатор в корпоративном мессенджере)
- временной диапазон, который эксперт готов тратить на обучение своих коллег

Менти:
- фио
- проект

Навык:
- название (например Java)
- категория
- квалификация
- описание (например как получил экспертизу, что читал, как использовал)

Категория:
- название (например язык программирования, технология или вид медитации)
- описание

Заявка:
- ссылка на менти
- ссылка на ментора
- временной диапазон для обучения (например каждую пятницу с 18 до 19)
- ссылка на навык ментора, которым хочет овладеть менти
- статус (может быть создана, принята, отклонена)


## API для управления моделями (CRUD)

## Ролевая модель
Пользователь с ролью`ADMIN` может редактировать модели, принимать и отклонять заявки. 
Пользователь с ролью `USER` может создавать заявку и просматривать существующие модели.

## API для создания и рассмотрения заявки
Пользователь может получить список менторов по интересующим категориям. 
Выбрать навык и временной диапазон и создать заявку на обучение.
Начальный статус заявки - создана.
Если заявка принимается, занятый временной диапазон ментора становится недоступен для будущих заявок.

## Тестовые данные
Миграционный скрипт `V0.2.4__add_test_data.sql` заполняет базу тестовыми значениями.
Там есть логин и пароль для тестового пользователя.

## Запуск проекта
```cmd
 docker-compose build
```
```cmd
 docker-compose up
```
Поднимется приложение на порту `8080` и БД для него. Чтобы начать использовать проект, изучите swagger спецификацию по адресу:
```url
http://localhost:8080/swagger-ui/index.html
```

>Stack of technologies used in my application:  

- PostgreSQL as a database  


- Jwt Token used to authenticate users with expiration time e.t.c  


- Test Containers  


- Swagger for information concerning method's usage


- MyBatis for interaction between app and a db

## Tutorial

Person can use this application two ways:

0. ***[must]*** Authorization
1. To get a mentor's expertise by creating **mentee**
2. To give expertise by creating **mentor**

### Authorization

1. Visit /api/auth/signup
2. Send a **POST** request with your *username*, *email*, *password* if you want to be a **mentee** or add *
   roles: ["ROLE_MODERATOR"]* if you want to be a **mentor**
3. Then visit /api/auth/signin
4. Send a **POST** request with your *username* and a *password*
5. You'll get a token which you must include in header in **any** request; Authorization: Bearer *[your_token]*

> Possible errors:
> - /signup throws 400 error w/o body?
>
> Check if you haven't added invalid role; Valid is only ROLE_MODERATOR

#### Creating mentee

1. You should create mentee entity;
2. Then list available categories you want to master
3. When you get familiar with categories you should create request with day, time and mentor id
4. Wait for approval or decline by chosen mentor
5. you're spectacular.

#### Creating mentor

1. You should create mentor entity
2. Then wait for 'PENDING' requests
3. Approve or Decline this request 
4. you're spectacular.

## FAQ

> *What if I want to be both mentor and mentee?*

This is totally acceptable and is implemented in an app.

> *Can I use app without signing up?*

No, app is configured in a way that doesn't allow usage without authorization. You can sign up and then sign in using
your jwt token returned to you.

However, you can visit swagger-ui with no authorization to get acquainted with methods and their usage.

> *Can I add skill/category myself?*

Unfortunately, no, skill and category adding are allowed only to Admin.

> *Can somebody accept/decline request instead of me?*

No, that is only allowed for authorized user with unique username *[or admin in case of an error occurrence]*

> *Is my password safely saved?*

Yes, application saves your password with encryption so that nobody can use it. When we get your log in request, we encrypt your typed password and compare it to the one saved in database.

