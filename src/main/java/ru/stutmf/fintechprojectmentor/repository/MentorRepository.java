package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import ru.stutmf.fintechprojectmentor.model.*;

import java.util.List;
import java.util.Optional;

@Mapper
public interface MentorRepository {

    void saveMentor(MentorDto mentor);

    void saveSkillsTimeContact(MentorDto mentor);

    List<Request> getPendingRequests(String username);

    List<MentorDto> findByCategories(List<Category> categoryList);

    void acceptRequest(int id, String username);

    void declineRequest(int id, String username);

    void update(MentorDto entity);

    void updateSkillsTimeContact(MentorDto entity);

    void saveMentorSkills(@Param("skillList") List<String> skillList, @Param("id") int id);

    void saveMentorContacts(@Param("contact") Contact contact, @Param("id") int id);

    void saveMentorTime(@Param("availableDays") AvailableDays schedule, @Param("id") int id);

    void deleteMentorSkill(@Param("skill") String skill, @Param("username") String name);

    Optional<MentorDto> find(int id);

    void delete(int id);
}
