package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.stutmf.fintechprojectmentor.model.User;

import java.util.Optional;

@Mapper
public interface UserRepository {
    void save(User user);

    void update(User user);

    void delete(String username);

    Optional<User>  findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
