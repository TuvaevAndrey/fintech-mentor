package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.stutmf.fintechprojectmentor.model.Category;

import java.util.List;
import java.util.Optional;

@Mapper
public interface CategoryRepository {
    void save(Category entity);

    void update(Category entity);

    void delete(String name);

    Optional<Category> find(String name);

    List<Category> findAllCategories();
}
