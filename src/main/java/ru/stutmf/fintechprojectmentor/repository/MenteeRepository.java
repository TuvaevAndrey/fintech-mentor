package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.stutmf.fintechprojectmentor.model.Mentee;

import java.util.Optional;

@Mapper
public interface MenteeRepository {
    void save(Mentee entity);

    void update(Mentee entity);

    void delete(int id);

    Optional<Mentee> find(int id);

    int getMyId(String name);
}
