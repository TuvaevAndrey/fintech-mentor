package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.stutmf.fintechprojectmentor.model.Role;

import java.util.Optional;

@Mapper
public interface RoleRepository {
    Optional<Role> findByName(String name);
}
