package ru.stutmf.fintechprojectmentor.repository;

import org.apache.ibatis.annotations.Mapper;
import ru.stutmf.fintechprojectmentor.model.Request;

import java.util.Optional;

@Mapper
public interface RequestRepository {
    void saveRequest(Request entity);

    void update(Request entity);

    void delete(int id);

    Optional<Request> find(int id);
}
