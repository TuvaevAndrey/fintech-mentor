package ru.stutmf.fintechprojectmentor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FintechProjectMentorApplication {

    public static void main(String[] args) {
        SpringApplication.run(FintechProjectMentorApplication.class, args);
    }

}
