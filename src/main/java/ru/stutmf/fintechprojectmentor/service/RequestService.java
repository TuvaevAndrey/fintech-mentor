package ru.stutmf.fintechprojectmentor.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.stutmf.fintechprojectmentor.model.Request;
import ru.stutmf.fintechprojectmentor.repository.RequestRepository;

import java.util.NoSuchElementException;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NOT_FOUND;

@AllArgsConstructor
@Service
public class RequestService {
    private static final Logger log = LoggerFactory.getLogger(RequestService.class);
    private final RequestRepository repository;

    public void save(Request request) {
        repository.saveRequest(request);
        log.info("New Request was saved");
    }

    public void update(Request request) {
        repository.update(request);
        log.info("Request with id = {} updated", request.getId());
    }

    @Transactional
    public void delete(int id) {
        checkIfEntityAbsent(id);

        repository.delete(id);
        log.info("Request with id = {} was deleted", id);
    }

    private void checkIfEntityAbsent(int id) {
        try {
            repository.find(id).orElseThrow();
        } catch (NoSuchElementException e) {
            log.info("Request with id = {} not found", id);
            throw ENTITY_NOT_FOUND.exception("");
        }
    }
}
