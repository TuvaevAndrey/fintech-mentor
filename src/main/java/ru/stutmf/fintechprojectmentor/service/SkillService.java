package ru.stutmf.fintechprojectmentor.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import ru.stutmf.fintechprojectmentor.model.Skill;
import ru.stutmf.fintechprojectmentor.repository.SkillRepository;

import java.util.List;

@AllArgsConstructor
@Service
public class SkillService {
    private static final Logger log = LoggerFactory.getLogger(MentorService.class);
    private final SkillRepository repository;

    public void save(Skill skill) {
        repository.save(skill);
        log.info("New Skill with name = {} saved", skill.getName());
    }

    public List<Skill> findAllSkills() {
        log.info("All skills returned");
        return repository.findAllSkills();
    }
}
