package ru.stutmf.fintechprojectmentor.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.stutmf.fintechprojectmentor.model.Category;
import ru.stutmf.fintechprojectmentor.repository.CategoryRepository;

import java.util.List;
import java.util.NoSuchElementException;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NOT_FOUND;

@AllArgsConstructor
@Service
public class CategoryService {

    private static final Logger log = LoggerFactory.getLogger(CategoryService.class);
    private final CategoryRepository repository;

    public void save(Category entity) {
        repository.save(entity);
        log.info("New Category with name = {} saved", entity.getName());
    }

    public List<Category> findAllCategories() {
        log.info("All categories returned");
        return repository.findAllCategories();
    }

    public void update(Category entity) {
        repository.update(entity);
        log.info("Category with name = {} updated", entity.getName());
    }

    @Transactional
    public void delete(String name) {
        checkIfEntityAbsent(name);

        repository.delete(name);
        log.info("Category with name = {} deleted", name);
    }

    private void checkIfEntityAbsent(String name) {
        try {
            repository.find(name).orElseThrow();
        } catch (NoSuchElementException e) {
            log.info("Category with name = {} not found", name);
            throw ENTITY_NOT_FOUND.exception("");
        }
    }
}
