package ru.stutmf.fintechprojectmentor.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.stutmf.fintechprojectmentor.model.*;
import ru.stutmf.fintechprojectmentor.repository.MentorRepository;

import java.util.List;
import java.util.NoSuchElementException;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NOT_FOUND;

@AllArgsConstructor
@Service
public class MentorService {
    private static final Logger log = LoggerFactory.getLogger(MentorService.class);
    private final MentorRepository repository;

    @Transactional
    public void save(MentorDto entity) {
        repository.saveMentor(entity);
        repository.saveSkillsTimeContact(entity);
        log.info("New Mentor with name = {} saved", entity.getName());
    }

    @Transactional
    public void update(MentorDto entity) {
        repository.update(entity);
        repository.updateSkillsTimeContact(entity);
        log.info("Mentor with name = {} updated", entity.getName());
    }

    public List<MentorDto> findByCategories(List<Category> categoryList) {
        log.info("Mentors specific to given categories returned");
        return repository.findByCategories(categoryList);
    }

    public List<Request> getPendingRequests(String username) {
        log.info("Pending requests to user = {} returned", username);
        return repository.getPendingRequests(username);
    }

    public void acceptRequest(int id, String username) {
        repository.acceptRequest(id, username);
        log.info("Request with id = {} accepted by {}", id, username);
    }

    public void declineRequest(int id, String username) {
        repository.declineRequest(id, username);
        log.info("Request with id = {} declined by {}", id, username);
    }

    public void saveMentorSkills(List<String> skillList, int id) {
        repository.saveMentorSkills(skillList, id);
    }

    public void saveMentorContacts(Contact contact, int id) {
        repository.saveMentorContacts(contact, id);
    }

    public void saveMentorTime(AvailableDays schedule, int id) {
        repository.saveMentorTime(schedule, id);
    }

    public void deleteMentorSkill(String skill, String name) {
        repository.deleteMentorSkill(skill, name);
    }

    @Transactional
    public void delete(int id) {
        checkIfEntityAbsent(id);

        repository.delete(id);
        log.info("Mentor with id = {} deleted", id);
    }

    private void checkIfEntityAbsent(int id) {
        try {
            repository.find(id).orElseThrow();
        } catch (NoSuchElementException e) {
            log.info("Mentor with id = {} not found", id);
            throw ENTITY_NOT_FOUND.exception("");
        }
    }
}
