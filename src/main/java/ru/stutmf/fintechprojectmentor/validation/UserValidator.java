package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.model.User;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserValidator implements ConstraintValidator<UserConstraint, User> {

    @Override
    public boolean isValid(User value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value.getUsername()) &&
                StringUtils.hasText(value.getPassword());
    }
}
