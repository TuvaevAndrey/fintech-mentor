package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.model.Category;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CategoryValidator implements ConstraintValidator<CategoryConstraint, Category> {

    @Override
    public boolean isValid(Category value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value.getName());
    }
}
