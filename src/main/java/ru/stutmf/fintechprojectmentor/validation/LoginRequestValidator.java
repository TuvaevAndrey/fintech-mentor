package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.payload.request.LoginRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LoginRequestValidator implements ConstraintValidator<LoginRequestConstraint, LoginRequest> {

    @Override
    public boolean isValid(LoginRequest value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value.getUsername()) &&
                StringUtils.hasText(value.getPassword());
    }
}
