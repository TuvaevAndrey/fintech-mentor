package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.model.Skill;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class SkillValidator implements ConstraintValidator<SkillConstraint, Skill> {

    @Override
    public boolean isValid(Skill value, ConstraintValidatorContext context) {
        return StringUtils.hasText(value.getName()) &&
                StringUtils.hasText(value.getCategory().getName());
    }
}
