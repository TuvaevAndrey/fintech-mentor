package ru.stutmf.fintechprojectmentor.validation;

import org.springframework.util.StringUtils;
import ru.stutmf.fintechprojectmentor.model.MentorDto;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class MentorValidator implements ConstraintValidator<MentorConstraint, MentorDto> {

    @Override
    public boolean isValid(MentorDto value, ConstraintValidatorContext context) {
        return value.getId() == 0 &&
                StringUtils.hasText(value.getName()) &&
                (StringUtils.hasText(value.getPhoneNumber()) || StringUtils.hasText(value.getWebsiteId())) &&
                value.getSkillList().stream().allMatch(skill -> StringUtils.hasText(skill.getName())) &&
                !value.getAvailableTime().isEmpty();
    }
}
