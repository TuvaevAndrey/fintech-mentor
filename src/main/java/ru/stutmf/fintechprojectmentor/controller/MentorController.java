package ru.stutmf.fintechprojectmentor.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.stutmf.fintechprojectmentor.model.*;
import ru.stutmf.fintechprojectmentor.service.MentorService;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NULL_FIELDS;

@CrossOrigin(origins = "*", maxAge = 3600)
@AllArgsConstructor
@RestController
@RequestMapping("/api/mentor")
public class MentorController {
    private static final Logger log = LoggerFactory.getLogger(MentorController.class);
    private final MentorService service;


    @Operation(
            description = "Create mentor to operate corresponding requests",
            summary = "Create Mentor"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(consumes = "application/json")
    public void saveMentor(@RequestBody @Validated MentorDto mentor, BindingResult errors, Principal principal) {
        if (errors.hasErrors()) {
            log.error("Saving failed: Mentor has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        mentor.setUsername(principal.getName());
        service.save(mentor);
    }


    @Operation(
            description = "List pending Requests only for !one! mentor; By default 'PENDING', must be set 'ACCEPTED' or 'DECLINED'",
            summary = "Get all requests to this Mentor by his ID"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @GetMapping(consumes = "application/json")
    public List<Request> getPendingRequests(Principal principal) {
        return service.getPendingRequests(principal.getName());
    }


    @Operation(
            description = "List all Mentors info by selecting few Categories; First step into making a request",
            summary = "List Mentors with their available time by Categories"
    )
    @GetMapping(value = "/findByCategories/{categories}")
    public @ResponseBody List<MentorDto> findByCategories(@PathVariable String[] categories) {
        return service.findByCategories(Arrays.stream(categories).map(Category::new).collect(Collectors.toList()));
    }


    @Operation(
            description = "Set status of request 'ACCEPTED' via request ID for authorized mentor only",
            summary = "Accept Request"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(value = "/acceptRequest", consumes = "application/json")
    public void acceptRequest (@RequestBody int id, Principal principal) {
        service.acceptRequest(id, principal.getName());
    }


    @Operation(
            description = "Set status of request 'DECLINED' via request ID for authorized mentor only",
            summary = "Decline Request"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(value = "/declineRequest", consumes = "application/json")
    public void declineRequest (@RequestBody int id, Principal principal) {
        service.declineRequest(id, principal.getName());
    }


    @Operation(
            description = "Update mentor by username; In case of changing, it will be updated automatically [cascade]",
            summary = "Update mentor"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PutMapping(consumes = "application/json")
    public void update(@RequestBody @Validated MentorDto mentor, Principal principal) {
        mentor.setUsername(principal.getName());
        service.update(mentor);
    }


    @Operation(
            description = "Save additional skills that were (forgotten about/not present) when saving mentor entity via Id",
            summary = "Save additional skills"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(value = "/{id}", consumes = "application/json")
    public void saveMentorSkills(@PathVariable("id") int id, @RequestBody List<String> skillList) {
        service.saveMentorSkills(skillList, id);
    }


    @Operation(
            description = "Rewrite mentor's credentials",
            summary = "Change mentor's contacts"
    )
    @PostMapping(value = "/contacts/{id}", consumes = "application/json")
    public void saveMentorContacts(@PathVariable("id") int id, @RequestBody Contact contact) {
        service.saveMentorContacts(contact, id);
    }


    @Operation(
            description = "Save additional mentor's time",
            summary = "Add mentor's time"
    )
    @PostMapping(value = "/addTime/{id}", consumes = "application/json")
    public void saveMentorTime(@PathVariable("id") int id, @RequestBody @Validated AvailableDays schedule) {
        service.saveMentorTime(schedule, id);
    }


    @Operation(
            description = "Save additional mentor's skill",
            summary = "Add mentor's skill"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @DeleteMapping(value = "/deleteMentorSkill/{name}")
    public void deleteMentorSkill(@PathVariable("name") String skill, Principal principal) {
        service.deleteMentorSkill(skill, principal.getName());
    }


    @Operation(
            description = "Delete mentor, all requests to this mentor will be deleted [cascade]",
            summary = "Delete Mentor"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public void deleteMentor(@PathVariable("id") int id) {
        service.delete(id);
    }
}
