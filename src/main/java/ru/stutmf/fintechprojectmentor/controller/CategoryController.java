package ru.stutmf.fintechprojectmentor.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.stutmf.fintechprojectmentor.model.Category;
import ru.stutmf.fintechprojectmentor.service.CategoryService;

import java.util.List;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NULL_FIELDS;

@AllArgsConstructor
@RestController
@RequestMapping("/api/category")
public class CategoryController {
    private static final Logger log = LoggerFactory.getLogger(CategoryController.class);
    private final CategoryService service;


    @Operation(
        description = "Create a category that is a must for Skill to be created",
        summary = "Create a Category"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(consumes = "application/json")
    public void saveCategory(@RequestBody @Validated Category category, BindingResult errors) {
        if (errors.hasErrors()) {
            log.info("Saving failed: Category has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        service.save(category);
    }


    @Operation(
        description = "List all available categories to choose a few and display all Mentors info with certain Categories",
        summary = "List all Categories"
    )
    @GetMapping(value = "/findAllCategories")
    public @ResponseBody
    List<Category> findAllCategories() {
        return service.findAllCategories();
    }


    @Operation(
        description = "Update category, name will be updated",
        summary = "Update category"
    )
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @PutMapping
    public void updateCategory(@RequestBody @Validated Category category) {
        service.update(category);
    }


    @Operation(
        description = "Delete a category, all Skills with this category will be deleted [cascade]",
        summary = "Delete a Category"
    )
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @DeleteMapping(value = "/{name}")
    public void deleteCategory(@PathVariable("name") String name) {
        service.delete(name);
    }
}
