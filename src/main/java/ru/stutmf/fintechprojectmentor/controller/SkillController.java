package ru.stutmf.fintechprojectmentor.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.stutmf.fintechprojectmentor.model.Skill;
import ru.stutmf.fintechprojectmentor.service.SkillService;

import java.util.List;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NULL_FIELDS;

@AllArgsConstructor
@RestController
@RequestMapping("/api/skill")
public class SkillController {
    private static final Logger log = LoggerFactory.getLogger(MentorController.class);
    private final SkillService service;


    @Operation(
            description = "Must have an existing category, otherwise 'Bad Request'",
            summary = "Save a skill"
    )
    @PreAuthorize("hasRole('MODERATOR') or hasRole('ADMIN')")
    @PostMapping(consumes = "application/json")
    public void saveSkill(@RequestBody @Validated Skill skill, BindingResult errors) {
        if (errors.hasErrors()) {
            log.info("Saving failed: Skill has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        service.save(skill);
    }


    @Operation(
            description = "List all Skills to choose for request",
            summary = "Get all Skills"
    )
    @GetMapping(value = "/findAllSkills")
    public @ResponseBody
    List<Skill> findAllSkills() {
        return service.findAllSkills();
    }
}
