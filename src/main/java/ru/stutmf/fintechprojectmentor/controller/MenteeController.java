package ru.stutmf.fintechprojectmentor.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.stutmf.fintechprojectmentor.model.Mentee;
import ru.stutmf.fintechprojectmentor.service.MenteeService;

import java.security.Principal;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NULL_FIELDS;

@AllArgsConstructor
@RestController
@RequestMapping("/api/mentee")
public class MenteeController {
    private static final Logger log = LoggerFactory.getLogger(MenteeController.class);
    private final MenteeService service;


    @Operation(
            description = "Create a mentee to have an id for making a request",
            summary = "Create a Mentee"
    )
    @PostMapping(consumes = "application/json")
    public void saveMentee(@RequestBody @Validated Mentee mentee, Principal principal, BindingResult errors) {
        if (errors.hasErrors()) {
            log.info("Saving failed: Mentee has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        mentee.setUsername(principal.getName());
        service.save(mentee);
    }


    @Operation (
            description = "Allowed only for a person with the same username: works by username",
            summary = "Update mentee"
    )
    @PutMapping(consumes = "application/json")
    public void updateMentee(@RequestBody @Validated Mentee mentee, Principal principal, BindingResult errors) {
        if (errors.hasErrors()) {
            log.info("Update failed: Mentee has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        mentee.setUsername(principal.getName());
        service.update(mentee);
    }


    @Operation(
            description = "Get this user's mentee id, used for creating a request, deleting mentee",
            summary = "Get mentee's id"
    )
    @GetMapping
    public int getMyId(Principal principal) {
        return service.getMyId(principal.getName());
    }


    @Operation(
            description = "Delete a mentee, all requests with this id will be deleted [cascade]",
            summary = "Delete a Mentee"
    )
    @DeleteMapping(value = "/{id}")
    public void deleteMentee(@PathVariable("id") int id) {
        service.delete(id);
    }
}
