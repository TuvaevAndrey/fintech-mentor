package ru.stutmf.fintechprojectmentor.controller;

import io.swagger.v3.oas.annotations.Operation;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.stutmf.fintechprojectmentor.model.Request;
import ru.stutmf.fintechprojectmentor.service.RequestService;

import static ru.stutmf.fintechprojectmentor.exception.ApplicationError.ENTITY_NULL_FIELDS;

@AllArgsConstructor
@RestController
@RequestMapping("/api/request")
public class RequestController {
    private static final Logger log = LoggerFactory.getLogger(RequestController.class);
    private final RequestService service;


    //TODO: make status not appear in swagger; make nano, seconds not appear; make isNull(status) fail request validation
    @Operation(
            description = "Create a request, by default status set to 'PENDING'",
            summary = "Create a Request"
    )
    @PostMapping(consumes = "application/json")
    public void saveRequest(@RequestBody @Validated Request request, BindingResult errors) {
        if (errors.hasErrors()) {
            log.info("Saving failed: Request has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        service.save(request);
    }


    @PutMapping(consumes = "application/json")
    public void updateRequest(@RequestBody @Validated Request request, BindingResult errors){
        if (errors.hasErrors()) {
            log.info("Saving failed: Request has wrong format");
            throw ENTITY_NULL_FIELDS.exception("");
        }
        service.update(request);
    }


    @Operation(
            description = "Delete a request",
            summary = "Delete a request"
    )
    @DeleteMapping(value = "/{id}")
    public void deleteRequest(@PathVariable("id") int id) {
        service.delete(id);
    }
}
