package ru.stutmf.fintechprojectmentor.payload.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.stutmf.fintechprojectmentor.validation.LoginRequestConstraint;

@Data
@AllArgsConstructor
@LoginRequestConstraint
public class LoginRequest {
    private String username;
    private String password;
}
