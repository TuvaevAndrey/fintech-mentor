package ru.stutmf.fintechprojectmentor.payload.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.stutmf.fintechprojectmentor.model.EnumRole;
import ru.stutmf.fintechprojectmentor.validation.SignupRequestConstraint;

import java.util.Set;

@Data
@NoArgsConstructor
@SignupRequestConstraint
public class SignupRequest {

    private Long id;

    private String username;
    private String email;
    private String password;
    private Set<EnumRole> roles;

    public SignupRequest (String username, String email, String password, Set<EnumRole> roles) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles = roles;
    }
}
