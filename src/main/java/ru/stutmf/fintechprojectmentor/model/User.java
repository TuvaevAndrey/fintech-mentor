package ru.stutmf.fintechprojectmentor.model;

import lombok.Data;
import ru.stutmf.fintechprojectmentor.validation.UserConstraint;

import java.util.HashSet;
import java.util.Set;

@Data
@UserConstraint
public class User {
    private Long id;

    private String username;
    private String email;
    private String password;

    private Set<Role> roles = new HashSet<>();

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }
    public User(Long id, String username, String email, String password, EnumRole role) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.password = password;
        this.roles.add(new Role(role));
    }
}
