package ru.stutmf.fintechprojectmentor.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import ru.stutmf.fintechprojectmentor.validation.SkillConstraint;

@Data
@NoArgsConstructor
@SkillConstraint
public class Skill {

    private String name;
    private Category category;

    private String qualification;
    private String description;

    public Skill(String name, Category category) {
        this.name = name;
        this.category = category;
    }
}
