package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
/*
@ContactConstraint
*/
public class Contact {
    private String phoneNumber;
    private String websiteId;
}
