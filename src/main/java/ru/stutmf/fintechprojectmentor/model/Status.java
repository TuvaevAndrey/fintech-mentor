package ru.stutmf.fintechprojectmentor.model;

public enum Status {
    PENDING, ACCEPTED, DECLINED
}
