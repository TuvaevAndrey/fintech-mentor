package ru.stutmf.fintechprojectmentor.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.stutmf.fintechprojectmentor.validation.RequestConstraint;

import java.time.DayOfWeek;
import java.time.LocalTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequestConstraint
public class Request {
    private int id;

    private int mentorId;
    private int menteeId;

    private String skillName;

    @ApiModelProperty(hidden = true)
    private Status status;

    private DayOfWeek day;
    private LocalTime start;
    private LocalTime end;
}
