package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AvailableDays {
    private Map<DayOfWeek, List<TimePair>> availableTime;
}
