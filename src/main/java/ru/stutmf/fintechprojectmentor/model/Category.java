package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.stutmf.fintechprojectmentor.validation.CategoryConstraint;

@Data
@AllArgsConstructor
@NoArgsConstructor
@CategoryConstraint
public class Category {

    private String name;

    private String description;

    public Category(String name) {
        this.name = name;
    }
}
