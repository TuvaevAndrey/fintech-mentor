package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TimePairString {
    private String start;
    private String end;
}
