package ru.stutmf.fintechprojectmentor.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.stutmf.fintechprojectmentor.validation.MentorConstraint;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
@MentorConstraint
public class MentorDto {
    private int id;

    private String name;
    private String surname;
    private String middleName;
    private String project;
    private List<Skill> skillList;

    private String phoneNumber;
    private String websiteId;

    private Map<DayOfWeek, List<TimePair>> availableTime;

    private String username;

    public MentorDto(String name, String phoneNumber, String websiteId, Map<DayOfWeek, List<TimePair>> time, List<Skill> skillList) {
        this.name = name;
        this.phoneNumber = phoneNumber;
        this.websiteId = websiteId;
        this.availableTime = time;
        this.skillList = skillList;
    }
}
