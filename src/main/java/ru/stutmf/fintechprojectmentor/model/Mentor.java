package ru.stutmf.fintechprojectmentor.model;

import lombok.Data;

import java.util.List;

@Data
public class Mentor {
    private int id;
    private String surname;
    private String name;
    private String middleName;

    private String project;
    private List<Skill> skillList;
    private String phoneNumber;
    private String websiteId;

    private List<AvailableDaysForMapper> availableDays;
}
