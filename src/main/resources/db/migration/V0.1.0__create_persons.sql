CREATE TABLE persons (
    person_id          INTEGER     PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    person_name        VARCHAR(64) NOT NULL,
    username           VARCHAR(64) NOT NULL,
    is_mentor          BOOLEAN     ,
    person_surname     VARCHAR(64) ,
    person_middle_name VARCHAR(64) ,
    person_project     VARCHAR(64) ,
    person_contact     VARCHAR(64)
);