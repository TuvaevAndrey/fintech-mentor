CREATE TABLE mentor_skills (
    skill_name VARCHAR(64) REFERENCES skills  ON DELETE CASCADE ON UPDATE CASCADE,
    mentor_id  INTEGER     REFERENCES persons ON DELETE CASCADE ON UPDATE CASCADE
)