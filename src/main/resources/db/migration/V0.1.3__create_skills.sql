CREATE TABLE skills (
    name          VARCHAR(64) PRIMARY KEY ,
    category      VARCHAR(64) REFERENCES  categories ON DELETE CASCADE ON UPDATE CASCADE ,
    qualification VARCHAR(64) ,
    description   VARCHAR(256)
);