CREATE TABLE roles (
    role_id   INTEGER     PRIMARY KEY,
    role_name VARCHAR(32) UNIQUE
);