CREATE TABLE mentor_contact (
    mentor_id    INTEGER     REFERENCES persons ON DELETE CASCADE ON UPDATE CASCADE,
    phone_number VARCHAR(32) ,
    website_id   VARCHAR(64)
);