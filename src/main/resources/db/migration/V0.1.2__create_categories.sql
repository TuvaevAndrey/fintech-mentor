CREATE TABLE categories (
    name        VARCHAR(64) PRIMARY KEY,
    description VARCHAR(256)
);