INSERT INTO roles
VALUES (1, 'ROLE_ADMIN'),
       (2, 'ROLE_MODERATOR'),
       (3, 'ROLE_USER');

INSERT INTO users(user_name, email, password)
VALUES ('test_user', 'test@test.test', '$2a$12$0uHkgR.G3JVihORTrPtxROQxl5qYVTcAg4DjpnUZVwJfAo2rcsCyG'); -- qwerty

INSERT INTO user_roles(user_name, role_id)
VALUES ('test_user', 1);