CREATE TABLE mentor_time (
    mentor_id  INTEGER     REFERENCES persons ON DELETE CASCADE ON UPDATE CASCADE,
    day        VARCHAR(64) ,
    start_time VARCHAR(64) ,
    end_time   VARCHAR(64) ,
    is_taken   BOOLEAN     DEFAULT 'FALSE'
);