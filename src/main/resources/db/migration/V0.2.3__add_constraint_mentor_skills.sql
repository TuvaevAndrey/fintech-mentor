ALTER TABLE mentor_skills
    ADD CONSTRAINT row_unique UNIQUE (skill_name, mentor_id);