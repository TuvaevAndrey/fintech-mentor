<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<mapper namespace="ru.stutmf.fintechprojectmentor.repository.MentorRepository">

    <resultMap id="MentorMap" type="ru.stutmf.fintechprojectmentor.model.Mentor">
        <id property="id" column="person_id"/>
        <result property="name" column="person_name"/>
        <result property="surname" column="person_surname"/>
        <result property="middleName" column="person_middle_name"/>
        <result property="project" column="person_project"/>
        <result property="phoneNumber" column="phone_number"/>
        <result property="websiteId" column="website_id"/>
        <collection property="skillList" javaType="list" ofType="String">
            <result column="skill_name"/>
        </collection>
        <collection property="availableDays" ofType="ru.stutmf.fintechprojectmentor.model.AvailableDaysForMapper">
            <id property="day" column="day"/>
            <collection property="time" javaType="list" ofType="ru.stutmf.fintechprojectmentor.model.TimePairString">
                <result property="start" column="start_time"/>
                <result property="end" column="end_time"/>
            </collection>
        </collection>
    </resultMap>

    <insert id="saveMentor" parameterType="ru.stutmf.fintechprojectmentor.model.MentorDto"
            useGeneratedKeys="true" keyColumn="person_id" keyProperty="id">
        INSERT INTO persons (person_name, username, is_mentor, person_surname,
                             person_middle_name, person_project)
        VALUES (#{name}, #{username}, 'TRUE', #{surname}, #{middleName}, #{project});
    </insert>

    <insert id="saveSkillsTimeContact" parameterType="ru.stutmf.fintechprojectmentor.model.MentorDto">
        INSERT INTO mentor_skills (skill_name, mentor_id) VALUES
        <foreach collection="skillList" item="item" index="index" separator=",">
            (#{item.name}, #{id})
        </foreach>;

        INSERT INTO mentor_time (mentor_id, day, is_taken, start_time, end_time)
        VALUES
        <foreach collection="availableTime.keySet()" item="mapKey" index="index" separator="),(" open="(" close=")">
            <foreach collection="availableTime.get(mapKey)" item="listEntry" index="index" separator="),(" close="">
                #{id}, #{mapKey}, FALSE, #{listEntry.start}, #{listEntry.end}
            </foreach>
        </foreach>;

        INSERT INTO mentor_contact (mentor_id, phone_number, website_id)
        VALUES (#{id}, #{phoneNumber}, #{websiteId});
    </insert>

    <select id="findByCategories" resultMap="MentorMap">
        SELECT person_id, person_name, person_surname, person_middle_name, person_project, phone_number, website_id,
               skill_name, category, qualification,
               categories.description, day, is_taken, start_time, end_time
        FROM persons
            JOIN mentor_skills ms on persons.person_id = ms.mentor_id
            JOIN skills s on s.name = ms.skill_name
            JOIN categories on categories.name = s.category
            JOIN mentor_time mt on persons.person_id = mt.mentor_id
            JOIN mentor_contact mc on persons.person_id = mc.mentor_id
        WHERE is_taken = FALSE AND is_mentor = TRUE AND
            <foreach collection="categoryList" item="category" separator="OR" open="(" close=")">
                categories.name = #{category.name}
            </foreach>
        ;
    </select>

    <select id="getPendingRequests" resultType="map">
        SELECT request_id, person_name, skill_name, day, start_time, end_time
        FROM requests
            JOIN persons p on requests.mentee_id = p.person_id
                WHERE p.username = #{username} AND status = 'PENDING';
    </select>

    <update id="acceptRequest">
        UPDATE requests
        SET status = 'ACCEPTED'
        FROM persons
        WHERE username = #{username} AND request_id = #{id};

        UPDATE mentor_time mt
        SET is_taken = TRUE
        FROM requests r
        WHERE mt.mentor_id = r.mentor_id AND mt.day = r.day AND mt.start_time = r.start_time AND mt.end_time = r.end_time;
    </update>

    <update id="declineRequest">
        UPDATE requests
        SET status = 'DECLINED'
        FROM persons
        WHERE username = #{username}
          AND request_id = #{id}
    </update>

    <select id="find" parameterType="java.lang.Integer" resultType="ru.stutmf.fintechprojectmentor.model.MentorDto">
        SELECT *
        FROM persons
        WHERE person_id = #{id}
          AND is_mentor = 'TRUE';
    </select>

    <update id="updateMentor" parameterType="ru.stutmf.fintechprojectmentor.model.MentorDto">
        UPDATE persons
        SET person_name        = #{name},
            person_surname     = #{surname},
            person_middle_name = #{middleName},
            person_project     = #{project}
        WHERE username = #{username};
    </update>

    <insert id="saveMentorSkills" parameterType="java.util.List">
        INSERT INTO mentor_skills (skill_name, mentor_id) VALUES
        <foreach collection="skillList" item="item" index="index" separator=",">
            (#{item.name}, #{id})
        </foreach>;
    </insert>

    <update id="saveMentorContacts" parameterType="ru.stutmf.fintechprojectmentor.model.Contact">
        UPDATE mentor_contact
        SET phone_number = #{phoneNumber},
            website_id   = #{websiteId}
        WHERE mentor_id = #{id};
    </update>

    <update id="saveMentorTime" parameterType="ru.stutmf.fintechprojectmentor.model.AvailableDaysForMapper">
        INSERT INTO mentor_time (mentor_id, day, is_taken, start_time, end_time)
        VALUES
        <foreach collection="availableTime.keySet()" item="mapKey" index="index" separator="),(" open="(" close=")">
            <foreach collection="availableTime.get(mapKey)" item="listEntry" index="index" separator="),(" close="">
                #{id}, #{mapKey}, FALSE, #{listEntry.start}, #{listEntry.end}
            </foreach>
        </foreach>
        ;
    </update>

    <delete id="deleteMentorSkill" parameterType="java.lang.String">
        DELETE
        FROM mentor_skills
            USING persons
        WHERE username = #{username}
          AND skill_name = #{skill}
          AND person_id = mentor_id;
    </delete>

    <delete id="delete" parameterType="java.lang.Integer">
        DELETE
        FROM persons
        WHERE person_id = #{id};
    </delete>

</mapper>