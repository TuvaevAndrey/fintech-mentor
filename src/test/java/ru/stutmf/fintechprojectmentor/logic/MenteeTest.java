package ru.stutmf.fintechprojectmentor.logic;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.stutmf.fintechprojectmentor.AbstractTest;
import ru.stutmf.fintechprojectmentor.model.Mentee;
import ru.stutmf.fintechprojectmentor.service.MenteeService;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class MenteeTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private MenteeService service;

    private final ObjectMapper mapper = new ObjectMapper();


    @WithMockUser
    @Test
    void testAddNewMenteeSuccess() throws Exception {
        var mentee = prepareValidMentee();
        var menteeJson = mapper.writeValueAsString(mentee);

        mockMvc.perform(post("/api/mentee")
                        .contentType("application/json")
                        .content(menteeJson))
                .andDo(print())
                .andExpect(status().isOk());

        var dbEntry = execSelectByUsernameQuery(mentee.getUsername());
        Assertions.assertEquals(mentee, dbEntry);
    }

    @WithMockUser
    @Test
    void testAddNewMenteeFail() throws Exception {
        var mentee = prepareInvalidMentee();
        var menteeJson = mapper.writeValueAsString(mentee);

        mockMvc.perform(post("/api/mentee")
                        .contentType("application/json")
                        .content(menteeJson))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByUsernameQuery(mentee.getUsername()));
    }

    @WithMockUser
    @Test
    void testUpdateMenteeSuccess() throws Exception {
        var mentee = prepareValidMentee();
        var menteeJson = mapper.writeValueAsString(mentee);

        mockMvc.perform(post("/api/mentee")
                        .contentType("application/json")
                        .content(menteeJson))
                .andDo(print())
                .andExpect(status().isOk());

        var updatedMentee = prepareUpdatedMentee();
        var updatedMenteeJson = mapper.writeValueAsString(updatedMentee);

        mockMvc.perform(put("/api/mentee")
                        .contentType("application/json")
                        .content(updatedMenteeJson))
                .andDo(print())
                .andExpect(status().isOk());

        var dbEntry = execSelectByUsernameQuery(updatedMentee.getUsername());
        Assertions.assertEquals(updatedMentee, dbEntry);
    }

    @WithMockUser
    @Test
    public void testGetMyIdSuccess() throws Exception {
        var mentee = prepareValidMentee();

        service.save(mentee);

        int menteeId = execSelectIdByUsernameQuery(mentee.getUsername()).getId();

        mockMvc.perform(get("/api/mentee"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(String.valueOf(menteeId)));
    }

    @WithMockUser
    @Test
    public void testDeleteMenteeSuccess() throws Exception{
        var mentee = prepareValidMentee();

        service.save(mentee);

        int menteeId = execSelectIdByUsernameQuery(mentee.getUsername()).getId();
        String urlDelete = "/api/mentee/" + menteeId;

        mockMvc.perform(delete(urlDelete))
                .andDo(print())
                .andExpect(status().isOk());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByUsernameQuery(mentee.getUsername()));
    }

    @WithMockUser
    @Test
    public void testDeleteMenteeFail() throws Exception{
        int menteeId = 0;

        String urlDelete = "/api/mentee/" + menteeId;

        mockMvc.perform(delete(urlDelete))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"message\":\"Entity not found: \"}"));
    }


    private Mentee execSelectByUsernameQuery(String username) {
        return jdbcTemplate.queryForObject(format
                        ("select person_name as name, username from public.persons where username='%s'", username),
                new BeanPropertyRowMapper<>(Mentee.class));
    }

    private Mentee execSelectIdByUsernameQuery(String username) {
        return jdbcTemplate.queryForObject(format
                        ("select person_id as id from public.persons where username='%s'", username),
                new BeanPropertyRowMapper<>(Mentee.class));
    }

    private Mentee prepareValidMentee() {
        return new Mentee("Steve", "user");
    }

    private Mentee prepareUpdatedMentee() {
        return new Mentee("Lika", "user");
    }


    private Mentee prepareInvalidMentee() {
        return new Mentee();
    }


    @BeforeEach
    public void fillUsers() {
        jdbcTemplate.execute("INSERT INTO users(user_name) VALUES ('user')");
    }

    @AfterEach
    public void clearCategories() {
        jdbcTemplate.execute("DELETE FROM persons; DELETE FROM users");
    }
}
