package ru.stutmf.fintechprojectmentor.logic;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import ru.stutmf.fintechprojectmentor.AbstractTest;
import ru.stutmf.fintechprojectmentor.model.Category;
import ru.stutmf.fintechprojectmentor.model.Skill;
import ru.stutmf.fintechprojectmentor.service.SkillService;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
public class SkillTest extends AbstractTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SkillService service;

    private final ObjectMapper mapper = new ObjectMapper();

    @BeforeEach
    public void fillCategories() {
        jdbcTemplate.execute("INSERT INTO categories VALUES ('Music', 'Tribe called quest i.e.')");
        jdbcTemplate.execute("INSERT INTO categories VALUES ('Unix', 'Debian, Arch Linux i.e.')");
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testAddNewSkillSuccess() throws Exception {
        var skill = prepareValidSkill();
        var skillJson = mapper.writeValueAsString(skill);

        mockMvc.perform(post("/api/skill")
                    .contentType("application/json")
                    .content(skillJson))
                .andDo(print())
                .andExpect(status().isOk());

        var dbEntry = execSelectByNameQuery(skill.getName());
        Assertions.assertEquals(skill, dbEntry);
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testAddNewSkillNullNameFail() throws Exception {
        var skill = prepareNullNameSkill();
        var skillJson = mapper.writeValueAsString(skill);

        mockMvc.perform(post("/api/skill")
                        .contentType("application/json")
                        .content(skillJson))
                .andDo(print())
                .andExpect(status().isBadRequest());

        Assertions.assertThrows(EmptyResultDataAccessException.class, () ->
                execSelectByNameQuery(skill.getName()));
    }

    @WithMockUser(roles = "MODERATOR")
    @Test
    public void testFindAllSkills() {
        var skill = prepareValidSkill();
        var skillNew = prepareNewValidSkill();

        List<Skill> skills = new ArrayList<>();
        skills.add(skill);
        skills.add(skillNew);

        service.save(skill);
        service.save(skillNew);

        var dbEntry = execSelectAllQuery();
        Assertions.assertEquals(skills, dbEntry);
    }


    private Skill execSelectByNameQuery(String name) {
        return jdbcTemplate.queryForObject(format("SELECT * FROM skills WHERE name ='%s'", name),
                new BeanPropertyRowMapper<>(Skill.class));
    }

    private List<Skill> execSelectAllQuery() {
        return jdbcTemplate.query("SELECT * FROM skills", new BeanPropertyRowMapper<>(Skill.class));
    }

    private Skill prepareValidSkill() {
        return new Skill("Jazz", new Category("Music"));
    }

    private Skill prepareNewValidSkill() {
        return new Skill("Linux", new Category("Unix"));
    }

    private Skill prepareNullNameSkill() {
        return new Skill("", new Category("Music"));
    }

    @AfterEach
    public void clearSkills(){
        jdbcTemplate.execute("DELETE FROM skills");
        jdbcTemplate.execute("DELETE FROM categories");
    }
}
